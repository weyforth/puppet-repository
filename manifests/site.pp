Exec { path => [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/', '/usr/local/bin/' ] }

# Standard modules

include ntp
include timezone

swap_file::files { 'default_swap':
	ensure       => present,
}

include user_accounts
include ssh_config
include zsh_env
include agent

# The rest

# ...

# Logging

include log

# These should be standard!
log::forwarder::input { '/var/log/syslog':
	type => 'syslog',
}

log::forwarder::input { '/var/log/auth.log':
	type => 'auth',
}

# Firewall

include firewall_rules
